"""
Date: 8/18/2020
Author: Yousef Javaherian
Details: Testing the code in a very simple synthetic dataset.(in this dataset points are in a half sphere)
"""
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import time
from mpl_toolkits.mplot3d import Axes3D
from Code.blocks import *

N = 70
r = 1
phi = np.random.uniform(0, 2 * np.pi, N)
theta = np.random.uniform(0, np.pi / 2, N)
X = np.vstack([r * np.sin(theta) * np.cos(phi), r * np.sin(theta) * np.sin(phi), r * np.cos(theta)]).T


start = time.time()
f, h, I, best_k, ll = kde(X, d=2)
print(f'kde block running time..............{time.time() - start}')

start = time.time()
K = sdp_solver(h, f, I, best_k)
print(f'sdp block running time..............{time.time() - start}')

start = time.time()
newX, lamda_dpm = svd(K, keep_energy_ratio=0.90)
print(f'svd block running time..............{time.time() - start}')

m = X.shape[0]
D = X.shape[1]
d = newX.shape[1]
newX_padded = np.pad(newX, ((0, 0), (0, 3 - d)), mode='constant', constant_values=0)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X[:, 0], X[:, 1], X[:, 2], label='Original data')
ax.scatter(newX_padded[:, 0], newX_padded[:, 1], newX_padded[:, 2], label='Transformed data')
ax.set_title("Data and it's transformation")
ax.legend(loc='best')
plt.savefig('../Figures/half-space-dataset.png', dpi=300, bbox_inches='tight')
plt.show()

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111)
ax.plot(np.arange(2, X.shape[0]), ll)
ax.axvline(x=best_k, ls='--')
ax.set_title("Leave One Out log likelihood plot")
ax.set_xlabel('k')
ax.set_ylabel('log likelihood')
plt.savefig('../Figures/half-space-looll.png', dpi=300, bbox_inches='tight')
plt.show()

pca = PCA(n_components=d)
pca.fit(X)
var_pca = [*pca.explained_variance_ratio_, 1 - np.sum(pca.explained_variance_ratio_)]
var_dpm = lamda_dpm[0:D] / np.sum(lamda_dpm)
fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111)
ax.plot(var_pca, marker='x', label='PCA')
ax.plot(var_dpm, marker='o', label='DPM')
ax.set_title("eigenvalue spectrum of matrix of inner products")
ax.set_xlabel('num')
ax.set_ylabel('eigenvalue')
ax.legend(loc='best')
plt.savefig('../Figures/half-space-eigval-spec.png', dpi=300, bbox_inches='tight')
plt.show()

# m = h.sh
# ape[0]
# k = I.shape[1]
# K = (Xnew - np.mean(Xnew, 0).reshape(1, -1)) @ (Xnew - np.mean(Xnew, 0).reshape(1, -1)).T
# D = np.repeat(np.diag(K).reshape(-1, 1), m, 1) + np.repeat(np.diag(K).reshape(1, -1), m, 0) - K - K.T
# E = 1 - D / np.repeat(h ** 2, m, 1)
# print(is_pos_def(K))
# print(np.allclose(np.sum(K), 0))
# repeated_date_1 = np.repeat(Xnew, m, axis=0)
# repeated_date_2 = np.tile(Xnew, (m, 1))
# distance_from_each_other_array = np.linalg.norm(repeated_date_2 - repeated_date_1, axis=1)
# distance_matrix = np.reshape(distance_from_each_other_array, (m, m))
# print(np.allclose(distance_matrix**2, D))
# print(np.all(E[np.repeat(np.arange(m), k), I.flatten()] >= 0))
# Ne = rdist.pdf(0, c=4)
# print(np.allclose(f, Ne/m * np.reshape(np.sum(np.reshape(E[np.repeat(np.arange(m), k), I.flatten()], (m, k)), axis=1), (m, 1))))
# print(f)
#
# print(Ne/m * np.reshape(np.sum(np.reshape(E[np.repeat(np.arange(m), k), I.flatten()], (m, k)), axis=1), (m, 1)))
