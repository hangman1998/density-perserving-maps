"""
Date: 8/18/2020
Author: Yousef Javaherian
Details: Testing the code in a subset of MNIST dataset.
"""

from time import time
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import offsetbox
from sklearn import manifold, datasets, decomposition
from Code.blocks import kde, sdp_solver, svd
import cvxpy as cp

# selecting 15 digits from each class
digits = datasets.load_digits(n_class=6)
X = digits.data
y = digits.target
X_g = np.zeros((6, 15, 64))
y_g = np.zeros((6, 15))
classes = np.unique(y)
for i, cls in enumerate(classes):
    X_cls = X[y == cls, :]
    chosen_indices = np.random.choice(np.arange(X_cls.shape[0]), 15)
    X_g[i, :, :] = X_cls[chosen_indices, :]
    y_g[i, :] = (y[y == cls])[chosen_indices]
X = np.reshape(X_g, (-1, 64))
y = y_g.flatten()


# Scale and visualize the embedding vectors
def plot_embedding(Xnew, title=None):
    x_min, x_max = np.min(Xnew, 0), np.max(Xnew, 0)
    Xnew = (Xnew - x_min) / (x_max - x_min)

    plt.figure()
    ax = plt.subplot(111)
    for i in range(Xnew.shape[0]):
        plt.text(Xnew[i, 0], Xnew[i, 1], str(int(y[i])), color=plt.cm.Set1(y[i] / 10.), fontdict={'weight': 'bold', 'size': 9})

    if hasattr(offsetbox, 'AnnotationBbox'):
        # only print thumbnails with matplotlib > 1.0
        shown_images = np.array([[1., 1.]])  # just something big
        for i in range(Xnew.shape[0]):
            dist = np.sum((Xnew[i] - shown_images) ** 2, 1)
            if np.min(dist) < 4e-3:
                # don't show points that are too close
                continue
            shown_images = np.r_[shown_images, [Xnew[i]]]
            imagebox = offsetbox.AnnotationBbox(offsetbox.OffsetImage(X[i].reshape(8, 8), cmap=plt.cm.gray_r), Xnew[i])
            ax.add_artist(imagebox)
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)

# Plot images of the digits
n_img_per_row = 9
img = np.zeros((10 * n_img_per_row, 10 * n_img_per_row))
for i in range(n_img_per_row):
    ix = 10 * i + 1
    for j in range(n_img_per_row):
        iy = 10 * j + 1
        img[ix:ix + 8, iy:iy + 8] = X[i * n_img_per_row + j].reshape((8, 8))

plt.imshow(img, cmap=plt.cm.binary)
plt.xticks([])
plt.yticks([])
plt.title('A selection from the 64-dimensional digits dataset')
plt.savefig('../Figures/mnist-digits-samples.png', dpi=300, bbox_inches='tight')
plt.show()
# ----------------------------------------------------------------------
# Running PCA
X_pca = decomposition.TruncatedSVD(n_components=2).fit_transform(X)
plot_embedding(X_pca, "Principal Components projection of the digits")
plt.savefig('../Figures/mnist-pca.png', dpi=300, bbox_inches='tight')
plt.show()
# Running t-SNE
tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
X_tsne = tsne.fit_transform(X)
plot_embedding(X_tsne, "t-SNE embedding of the digits")
plt.savefig('../Figures/mnist-tsne.png', dpi=300, bbox_inches='tight')
plt.show()
for k in [5, 10, 15, 20, 25]:
    # Running DPM
    start = time()
    f, h, I, best_k, ll = kde(X, fixed_k=k, d=1)
    print(f'kde block running time..............{time() - start}')
    # ['CVXOPT', 'ECOS', 'GLPK', 'GLPK_MI', 'OSQP', 'SCS']
    start = time()
    K = sdp_solver(h, f, I, best_k)
    print(f'sdp block running time..............{time() - start}')

    start = time()
    X_dpm, _ = svd(K, d=2)
    print(f'svd block running time..............{time() - start}')
    plot_embedding(X_dpm, 'DPM embedding of the digits')
    plt.savefig(f'../Figures/mnist-dpm-k-{k}.png', dpi=300, bbox_inches='tight')
    plt.show()
