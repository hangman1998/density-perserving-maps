"""
Date: 8/16/2020
Author: Yousef Javaherian - Mohammad Hossein Heidari
Details: Implementation of algorithm explained in the paper "Density preserving maps" which
is Chapter 3 of the book "Manifold learning theory and applications by Yunqian Ma Yun Fu"
"""
import numpy as np
import cvxpy as cp
from scipy.stats import rdist
from copy import copy


# kde block
def kde(X: np.ndarray, d: int,  fixed_k=None):
    m = X.shape[0]  # number of data
    # calculated distance matrix
    repeated_date_1 = np.repeat(X, m, axis=0)
    repeated_date_2 = np.tile(X, (m, 1))
    distance_matrix = np.reshape(np.linalg.norm(repeated_date_2 - repeated_date_1, axis=1), (m, m))
    # sort the distance matrix through the row
    sorted_distance_matrix = np.sort(distance_matrix, axis=1)
    ll = np.zeros(m - 2)
    if fixed_k is None:
        # select best k
        for k in np.arange(2, m):
            h = sorted_distance_matrix[:, k].reshape((-1, 1)) + 1e-6
            f = (h ** -d) * np.sum(rdist.pdf(distance_matrix / h, c=4) * (1 - np.eye(m)), axis=1) / (m - 1)
            ll[k - 2] = np.sum(np.log(f))
        best_k = np.argmax(ll) + 2
    else:
        best_k = fixed_k
    h_best = sorted_distance_matrix[:, best_k].reshape((-1, 1))
    best_f = np.mean(rdist.pdf(distance_matrix / h_best, c=4), axis=1).reshape((-1, 1))
    Q, I = np.nonzero(distance_matrix <= h_best)
    # simple dirty bug fix:
    keep = np.ones_like(I, dtype=bool)
    run = 0
    current_row = 0
    for i in range(Q.shape[0]):
        if Q[i] == current_row:
            if run >= best_k:
                keep[i] = False
            run = run + 1
        else:
            run = 1
            current_row = Q[i]
    newI = I[keep]
    print(m * best_k)
    print(newI.shape)
    print(I.shape)
    assert newI.shape[0] == m * best_k

    return best_f, h_best, newI, best_k, ll


# semi-definite programming solver
def sdp_solver(h: np.ndarray, f: np.ndarray, I: np.ndarray, k):
    m = h.shape[0]
    Ne = rdist.pdf(0, c=4)
    assert h.shape == (m, 1) and f.shape == (m, 1)
    K = cp.Variable((m, m), PSD=True, name='K')
    E = cp.Variable((m, m), name='E')
    D = cp.Variable((m, m), name='D')
    sliced_E = cp.Variable((m, k), name='sliced_E')
    constraints = []
    # constraints += [K >> 0]
    constraints += [cp.sum(cp.sum(K, 1), 0) == 0]
    temp = cp.vstack([cp.diag(K) for i in range(m)]) - K
    constraints += [D == temp + temp.T]
    constraints += [E == 1 - D / np.repeat(h ** 2, m, 1)]
    constraints += [sliced_E == cp.reshape(E[np.repeat(np.arange(m), k), I], (m, k), order='C')]
    constraints += [sliced_E >= 0]
    constraints += [f == Ne / m * cp.reshape(cp.sum(sliced_E, axis=1), (m, 1), order='C')]
    prob = cp.Problem(cp.Maximize(cp.trace(K)), constraints)
    prob.solve(solver=cp.SCS, verbose=True)
    print(prob.status)
    return K.value


# svd block
def svd(K, d=None, keep_energy_ratio=0.95):
    m = K.shape[0]
    lamda, V = np.linalg.eigh(K)
    lamda = np.flip(lamda)
    # print(lamda)
    V = np.flip(V, axis=1)
    if d is None:
        d = np.nonzero(np.cumsum(lamda) >= keep_energy_ratio * np.sum(lamda))[0][0] + 1
    Sigma = np.zeros((d, m))
    Sigma[:, :d] = np.diag(np.sqrt(lamda[0:d]))
    res = Sigma @ V.T
    return res.T, lamda
