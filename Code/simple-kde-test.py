import numpy as np
from scipy.stats import rdist
from scipy.stats import norm
import matplotlib.pyplot as plt

x = np.array([-3, -2.5, -0.75, 1.3, 2.1, 4.5, 4.8, 2.3, -0.41, -0.5, 0.22])
x = np.random.normal(0, 1, 11)

plt.scatter(x, np.zeros_like(x), s=20, marker='x')
# finding best K
best_k = 2
max_LL = -np.inf
for k in np.arange(2, 11):
    distance = np.abs(np.repeat(x.reshape((1, -1)), 11, 0) - np.repeat(x.reshape((-1, 1)), 11, 1))
    h = np.sort(distance, axis=1)[:, k].reshape((-1, 1))
    h = np.repeat(h, 11, 1)
    # print(h)
    # print(distance)
    # print(distance)
    f = np.sum(1/h * rdist.pdf(distance / h, c=4) * (1 - np.eye(11)), axis=1) / 10
    # print(1/h * rdist.pdf(distance / h, c=4))
    LL = np.sum(np.log(f), axis=0)
    if LL >= max_LL:
        max_LL = LL
        best_k = k
print(best_k)
h1 = 1.2
t = np.linspace(-5, 5, 100)
distance = np.abs(np.repeat(x.reshape((1, -1)), 100, 0) - np.repeat(t.reshape((-1, 1)), 11, 1))
print(distance.shape)
h2 = np.sort(distance, axis=1)[:, k].reshape((-1, 1))
h2 = np.repeat(h2, 11, 1)
#
#
# # # print(h.shape)
# #
f1 = np.mean(1/h1 * rdist.pdf(distance / h1, c=4), axis=1)
f2 = np.mean(1/h2 * rdist.pdf(distance / h2, c=4), axis=1)
# # # d = rdist.pdf(t, c=4)
# # #
plt.plot(t, f1, t, f2, t, norm.pdf(t))
plt.show()
# # print(rdist.pdf(t, c=4))
# # a = np.random.normal(0, 1, 10)
# # print(np.sort(a))